package org.dnyanyog.dto.response;

import org.dnyanyog.dto.request.StudentRequest;
import org.springframework.stereotype.Component;

@Component
public class StudentResponse {
	private String status;
	private String massage;
	private StudentData data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMassage() {
		return massage;
	}
	public void setMassage(String massage) {
		this.massage = massage;
	}
	public StudentData getData() {
		return data;
	}
	public void setData(StudentData data) {
		this.data = data;
	}
	
	
	
}
