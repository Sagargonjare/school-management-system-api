package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class UserResponse {
	private String status;
	private String message;
	private UserData data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public UserData getData() {
		return data;
	}
	public void setData(UserData data) {
		this.data = data;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
