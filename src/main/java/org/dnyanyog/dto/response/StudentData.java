package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class StudentData {
	private long grnNumber;
	private String studentName;
	private String fatherName;
	private String motherName;
	private String lastName;
	private String parentMobile;
	private String adress;
	private String birthDate;
	private String gender;
	private String appliedClass;
	private String priviousSchool;
	private String studentMobile;
	private String studentEmail;
	public long getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(long grnNumber) {
		this.grnNumber = grnNumber;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getParentMobile() {
		return parentMobile;
	}
	public void setParentMobile(String parentMobile) {
		this.parentMobile = parentMobile;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAppliedClass() {
		return appliedClass;
	}
	public void setAppliedClass(String appliedClass) {
		this.appliedClass = appliedClass;
	}
	public String getPriviousSchool() {
		return priviousSchool;
	}
	public void setPriviousSchool(String priviousSchool) {
		this.priviousSchool = priviousSchool;
	}
	public String getStudentMobile() {
		return studentMobile;
	}
	public void setStudentMobile(String studentMobile) {
		this.studentMobile = studentMobile;
	}
	public String getStudentEmail() {
		return studentEmail;
	}
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	
	
}
