package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class LoginRequest {
	private String password;
	private String mobile;
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
