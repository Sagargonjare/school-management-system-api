package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table
public class Student {
	@Id
	@GeneratedValue
	private long grnNumber;
	@Column
	private String studentName;
	@Column
	private String fatherName;
	@Column
	private String motherName;
	@Column
	private String lastName;
	@Column(unique=true)
	private String parentMobile;
	@Column
	private String adress;
	@Column
	private String birthDate;
	@Column
	private String gender;
	@Column
	private String appliedClass;
	@Column
	private String priviousSchool;
	@Column
	private String studentMobile;
	@Column
	private String studentEmail;
	public long getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(long grnNumber) {
		this.grnNumber = grnNumber;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getParentMobile() {
		return parentMobile;
	}
	public void setParentMobile(String parentMobile) {
		this.parentMobile = parentMobile;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAppliedClass() {
		return appliedClass;
	}
	public void setAppliedClass(String appliedClass) {
		this.appliedClass = appliedClass;
	}
	public String getPriviousSchool() {
		return priviousSchool;
	}
	public void setPriviousSchool(String priviousSchool) {
		this.priviousSchool = priviousSchool;
	}
	public String getStudentMobile() {
		return studentMobile;
	}
	public void setStudentMobile(String studentMobile) {
		this.studentMobile = studentMobile;
	}
	public String getStudentEmail() {
		return studentEmail;
	}
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}
	
	
}
