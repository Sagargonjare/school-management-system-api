package org.dnyanyog.controller;

import org.dnyanyog.dto.request.AddUserRequest;
import org.dnyanyog.dto.response.UserResponse;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class UserController {
	@Autowired
	UserService userService;
	@PostMapping(path = "directory/api/v1/users",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<UserResponse> addUser(@RequestBody AddUserRequest request) {
		return userService.saveData(request);

	}
}
