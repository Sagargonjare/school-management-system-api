package org.dnyanyog.service;

import java.util.List;

import org.dnyanyog.dto.request.StudentRequest;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.StudentData;
import org.dnyanyog.dto.response.StudentResponse;
import org.dnyanyog.entity.Student;
import org.dnyanyog.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
	@Autowired
	StudentRepository studentRepository;
	@Autowired
	Student student;
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	StudentResponse studentResponse;
	@Autowired
	StudentResponse response;
	@Autowired 
	List<Student> students;
	
	private ResponseEntity<StudentResponse> getConflictSignUpResponse() {
		StudentResponse response = new StudentResponse();
		response.setStatus("error");
		response.setMassage(" mobile number of Parent is already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}
	
	public ResponseEntity <StudentResponse> saveData(StudentRequest request) {
		response=new StudentResponse();
		response.setData(new StudentData());
		
		if (null != studentRepository.findByParentMobile(request.getParentMobile()))
			return getConflictSignUpResponse();

		
		student =new Student();
		student.setAppliedClass(request.getAppliedClass());
		student.setAdress(request.getAdress());
		student.setBirthDate(request.getBirthDate());
		student.setFatherName(request.getFatherName()); 
		student.setGender(request.getGender()); 
		student.setGrnNumber(request.getGrnNumber());
		student.setLastName(request.getLastName());
		student.setMotherName(request.getMotherName());
		student.setParentMobile(request.getParentMobile()); 
		student.setPriviousSchool(request.getPriviousSchool());
		student.setStudentEmail(request.getStudentEmail()); 
		student.setStudentMobile(request.getStudentMobile()); 
		student.setStudentName(request.getStudentName());
		
		
		student=studentRepository.save(student);
		
		response.setStatus("Success");
		response.setMassage("Student Added Successfully");
		
		response.getData().setAdress(student.getAdress());
		response.getData().setAppliedClass(student.getAppliedClass());
		response.getData().setBirthDate(student.getBirthDate());
		response.getData().setFatherName(student.getFatherName());
		response.getData().setGender(student.getGender());
		response.getData().setGrnNumber(student.getGrnNumber());
		response.getData().setLastName(student.getLastName());
		response.getData().setMotherName(student.getMotherName());
		response.getData().setParentMobile(student.getParentMobile());
		response.getData().setPriviousSchool(student.getPriviousSchool());
		response.getData().setStudentEmail(student.getStudentEmail());
		response.getData().setStudentName(student.getStudentName());
		
		
		return ResponseEntity .status(HttpStatus.CREATED).body(response);

	}
}

























