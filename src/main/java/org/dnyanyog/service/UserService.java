package org.dnyanyog.service;

import java.util.List;

import org.dnyanyog.dto.request.AddUserRequest;

import org.dnyanyog.dto.response.UserData;
import org.dnyanyog.dto.response.UserResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	Users users;
	
	@Autowired
	UserResponse userResponse;
	@Autowired
	UserResponse response;
	@Autowired 
	List<Users> user;
	
	
	private ResponseEntity<UserResponse> getConflictSignUpResponse() {
		UserResponse response = new UserResponse();
		response.setStatus("error");
		response.setMessage("  number of Parent is already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}
	
	
	public ResponseEntity<UserResponse> saveData(AddUserRequest request) {
		response=new UserResponse();
		response.setData(new UserData());
		
		if (null != userRepository.findByMobile(request.getMobile()))
			return getConflictSignUpResponse();

		
		users=new Users();
		users.setMobile(request.getMobile());
		users.setPassword(request.getPassword());
		users.setUserRole(request.getUserRole());
		users.setId(request.getId());

		users=userRepository.save(users);
		
		response.setStatus("Success");
		response.setMessage("User Added Successfully");
		
		response.getData().setMobile(users.getMobile());
		response.getData().setPassword(users.getPassword());
		response.getData().setUserRole(users.getUserRole());
		//response.getData().set
		return ResponseEntity .status(HttpStatus.CREATED).body(response);

	}
	
	
}
